import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { TodoService } from '../service/todo.service';
import { ITodo } from '../shared/models';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  newTodoTitle: string = '';
  newTodoTask: string = '';

  constructor(private todoService: TodoService) {
  }

  ngOnInit() {
  }

  create() {
    this.todoService.create(this.newTodoTitle, this.newTodoTask);
  }

}
