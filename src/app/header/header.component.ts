import { Component, OnInit } from '@angular/core';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public counter: number;

  constructor(private todoService: TodoService) {
    this.todoService.counterEvent$.subscribe(count => this.counter++);
  }

  ngOnInit() {
    this.counter = 0;
  }

}
