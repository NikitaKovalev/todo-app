import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import { ITodo } from '../shared/models';
import { TodoService } from '../service/todo.service';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.scss']
})
export class TodoItemComponent implements OnInit {
  @Input() todo: ITodo;
  @Input() index: number;
  @Output() delete: EventEmitter<ITodo> = new EventEmitter<ITodo>();

  constructor(private todoService: TodoService) {
  }

  ngOnInit() {
  }

  onDelete(): void {
    this.delete.emit(this.todo);
  }

  onToggle(): void {
    this.todoService.toggle(this.todo);
  }

  colorEvenItem() {
    return this.index % 2 === 0 ? 'colored' : null;
  }

  complite() {
    return this.todo.completed ? 'check_box' : 'check_box_outline_blank';
  }
}
