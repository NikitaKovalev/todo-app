import { Component, OnInit } from '@angular/core';
import { ITodo } from '../shared/models/todo';
import { TodoService } from '../service/todo.service';

@Component({
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  todos: Array<ITodo>;

  constructor(private todoService: TodoService) {
    this.todos = [];
  }

  ngOnInit() {
    this.todos = this.todoService.getData();
  }

  delete(todo: ITodo): void {
    this.todoService.deleteTodo(todo);
  }

}
