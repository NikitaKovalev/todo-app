import { EventEmitter, Injectable } from '@angular/core';
import { TODOS } from '../shared/data';
import { ITodo } from '../shared/models';

@Injectable()
export class TodoService {
  todos: Array<ITodo> = TODOS;
  counterEvent$: EventEmitter<any> = new EventEmitter<any>();

  constructor() {
  }

  getData(): Array<ITodo> {
    const todoList = [...this.todos]; /*распаковка объекта, и присваивание переменной, (копия)*/
    return todoList;
  }

  create(title, description): void {
    const todo: ITodo = {title: title, description: description};

    this.todos.unshift(todo);
    this.counterEvent$.emit(todo);
  }

  toggle(todo: ITodo): void {
    todo.completed = !todo.completed;
  }

  deleteTodo(todo: ITodo): void {
    const index = this.todos.indexOf(todo);

    if (index !== -1) {
      this.todos.splice(index, 1);
    }
  }
}
